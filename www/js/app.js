// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ionMdInput', 'nfcFilters', 'ngCordova'])

.factory('nfcService', function($rootScope, $ionicPlatform, $filter, $state, $http, $ionicViewService) {

    var tag = {};

    $ionicPlatform.ready(function() {
        try {
            nfc.addNfcVListener(function(nfcEvent) {
                // alert("reading NfcV " + $filter('bytesToString')(nfcEvent.tag.block).replace('#', ''));
                // $cordovaToast.show('Book No : ' + $filter('bytesToString')(nfcEvent.tag.block).replace('#', ''), 'long', 'center');

                window.plugins.toast.show('Book No : ' + $filter('bytesToString')(nfcEvent.tag.block).replace('#', ''), 'long', 'bottom', function(a) {
                    console.log('toast success: ' + a)
                }, function(b) {
                    alert('toast error: ' + b)
                });
                console.log(JSON.stringify(nfcEvent.tag, null, 4));
                $rootScope.$apply(function() {
                    angular.copy(nfcEvent.tag, tag);
                    // $state.go("app.new");
                    var x = $filter('bytesToString')(nfcEvent.tag.block).replace(/\0/g, '').replace('#', '');
                    $http.get($rootScope.serviceUrl + '/checkoutapi/api/book?barcode=' + x).
                    success(function(data, status, headers, config) {
                        console.log(data);
                        $ionicViewService.nextViewOptions({
                            disableBack: true
                        });
                        $state.go("app.new", {
                            "bookId": x
                        });
                    }).
                    error(function(data, status, headers, config) {
                        console.log(data);
                        // alert(status);
                    });
                });
            }, function() {
                console.log("Listening for NfcV Tags.");
                // alert("Listening for NfcV Tags.");
                // $cordovaToast.show('Listening for NFC Tags.', 'short', 'center');
                window.plugins.toast.show('Listening for NFC Tags.', 'short', 'bottom', function(a) {
                    console.log('toast success: ' + a)
                }, function(b) {
                    alert('toast error: ' + b)
                });

            }, function(reason) {
                // alert("Error adding NfcV Listener " + reason);
                window.plugins.toast.show('Error adding NFC Listener ' + reason, 'short', 'bottom', function(a) {
                    console.log('toast success: ' + a)
                }, function(b) {
                    alert('toast error: ' + b)
                });

            });
        } catch (e) {
            // alert(e);
        }

    });

    return {
        tag: tag,

        clearTag: function() {
            angular.copy({}, this.tag);
        }
    };
})

.run(function($ionicPlatform, $rootScope) {

    var serviceUrl = window.localStorage.getItem("serviceUrl");
    console.log('service url : ' + serviceUrl);
    if (serviceUrl == null || serviceUrl == "" || serviceUrl ==undefined) {
        serviceUrl = "http://pen1demo.ddns.net:6115";
        window.localStorage.setItem("serviceUrl", serviceUrl);
    } 

     $rootScope.serviceUrl = serviceUrl;

    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})



.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })
        .state('app.playlists', {
            url: '/playlists',
            views: {
                'menuContent': {
                    templateUrl: 'templates/playlists.html',
                    controller: 'PlaylistsCtrl'
                }
            }
        })
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
        })
        .state('app.new', {
            url: '/new/:bookId',
            views: {
                'menuContent': {
                    templateUrl: 'templates/new.html',
                    controller: 'NewCtrl'
                }
            }
        })
        .state('app.single', {
            url: '/playlists/:bookId',
            views: {
                'menuContent': {
                    templateUrl: 'templates/playlist.html',
                    controller: 'PlaylistCtrl'
                }
            }
        }).state('app.slip', {
            url: '/slip',
            views: {
                'menuContent': {
                    templateUrl: 'templates/slip.html',
                    controller: 'SlipCtrl'
                }
            }
        }).state('setting', {
            url: '/setting',
            templateUrl: 'templates/setting.html',
            controller: 'SettingCtrl'
        });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
});
