angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $state, nfcService) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    $scope.gotoCheckin = function(bookId) {
        
        $state.go("app.new", {
            "bookId": bookId
        });
    };

    $scope.doLogout = function() {
        console.log('logout');
        window.localStorage.clear();
        $state.go("login");
    };
})

.controller('PlaylistsCtrl', function($scope, $http, $rootScope) {

    $scope.loadBookList = function() {
        var username = window.localStorage.getItem("username");
        // $http.get('http://aekinjoy.ddns.net:6115/checkoutapi/api/checkoutlist?user=' + $rootScope.username).

        console.log($rootScope.transaction);
        if ($rootScope.transaction != undefined && $rootScope.transaction != "") {
            $http.get($rootScope.serviceUrl + '/CheckOutApi/api/checkoutlist?user=' + username + '&transid=' + $rootScope.transaction).
            success(function(data, status, headers, config) {
                console.log(data);
                $scope.playlists = data;
            }).
            error(function(data, status, headers, config) {
                console.log(data);
                $scope.playlists = undefined;
                // alert(status);
            });
        } else {
            $scope.playlists = undefined;
        }
    };

    $scope.loadBookList();

    $scope.clearList = function() {
        $scope.playlists = undefined;
        $rootScope.transaction = undefined;
    }

    

    $rootScope.$on('reload', function(event, args) {
        console.log('xxx');
        $scope.loadBookList();
    });

})

.controller('SlipCtrl', function($scope, $stateParams, $http) {
    try {
        $scope.slip = cordova.file.externalRootDirectory + "slip.jpg";
    } catch(e) {
        // statements
        console.log(e);
    }
})

.controller('PlaylistCtrl', function($scope, $stateParams, $http, $rootScope) {
    $scope.book = undefined;
    $scope.loadBookInfo = function(bookId) {
        $http.get($rootScope.serviceUrl + '/checkoutapi/api/book?barcode=' + bookId).
        success(function(data, status, headers, config) {
            console.log(data);
            $scope.book = data;
        }).
        error(function(data, status, headers, config) {
            console.log(data);
            $scope.book = undefined;
        });
    };
    $scope.loadBookInfo($stateParams.bookId);
})

.controller('NewCtrl', function($scope, $stateParams, $state, $timeout, $http, $rootScope, $cordovaFileTransfer, $ionicViewService) {
    console.log($stateParams.bookId);

    $scope.book = undefined;

    $scope.loadBookInfo = function(bookId) {
        $http.get($rootScope.serviceUrl + '/checkoutapi/api/book?barcode=' + bookId).
        success(function(data, status, headers, config) {
            console.log(data);
            $scope.book = data;
            // $cordovaToast.show('Book No : ' + $scope.book.BookNo, 'short', 'center')
            try {
                window.plugins.toast.show('Book No : ' + $scope.book.BookNo, 'short', 'bottom', function(a) {
                    console.log('toast success: ' + a)
                }, function(b) {
                    alert('toast error: ' + b)
                });

            } catch (e) {
                // statements
                console.log(e);
            }

        }).
        error(function(data, status, headers, config) {
            console.log(data);
            $scope.book = undefined;
            // $cordovaToast.show('Book not found', 'short', 'center');
            try {
                window.plugins.toast.show('Book ' + bookId + ' not found', 'short', 'bottom', function(a) {
                    console.log('toast success: ' + a)
                }, function(b) {
                    alert('toast error: ' + b)
                });

            } catch (e) {
                // statements
                console.log(e);
            }

        });
    };

    console.log($stateParams.bookId);
    if ($stateParams.bookId != undefined && $stateParams.bookId != "") {
        $scope.loadBookInfo($stateParams.bookId);
    }

    $scope.checkOut = function(bookId) {
        var username = window.localStorage.getItem("username");
        var url = $rootScope.serviceUrl + '/checkoutapi/api/checkout?user=' + username + '&barcode=' + bookId + '&transid=';

        if ($rootScope.transaction != undefined && $rootScope.transaction != "") {
            url += $rootScope.transaction;
        }

        $http.get(url).
        success(function(data, status, headers, config) {
            console.log(data);
            // alert(data.Status);
            if (data.Status.toLowerCase() == 'success') {

                console.log(data.TransactionID);
                if (data.TransactionID != undefined && data.TransactionID != "") {
                    $rootScope.transaction = data.TransactionID;
                }

                $scope.saveSlip();

                $ionicViewService.nextViewOptions({
                            disableBack: true
                        });
                $state.go("app.playlists");
                $rootScope.$emit('reload', [1, 2, 3]);
            }
        }).
        error(function(data, status, headers, config) {
            console.log(data);
        });
    }

    $scope.saveSlip = function() {
        var message = "";
        try {

            var url = $rootScope.serviceUrl + "/CheckOutApi/api/checkoutimg?user=005&transid=201601281917601";
            message = "url";
            var targetPath = cordova.file.externalRootDirectory + "slip.jpg";
            message = "targetPath " + targetPath;
            var trustHosts = true;
            var options = {};
            message = "save to " + targetPath;
            $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
                .then(function(result) {
                    window.plugins.toast.show('Save success to ' + cordova.file.externalRootDirectory, 'long', 'bottom', function(a) {
                        console.log('toast success: ' + a)
                    }, function(b) {
                        alert('toast error: ' + b)
                    });

                }, function(err) {
                    window.plugins.toast.show('Save failed', 'long', 'bottom', function(a) {
                        console.log('toast success: ' + a)
                    }, function(b) {
                        alert('toast error: ' + b)
                    });
                }, function(progress) {

                });
        } catch (e) {
            // statements
            window.plugins.toast.show(message + e.message, 'long', 'bottom', function(a) {
                console.log('toast success: ' + a)
            }, function(b) {
                alert('toast error: ' + b)
            });
        }

    }


})


.controller('SettingCtrl', function($scope, $stateParams, $rootScope, $state) {
    $scope.setting = {
        serviceUrl: $rootScope.serviceUrl
    }

    $scope.save = function() {
        window.localStorage.setItem("serviceUrl",  $scope.setting.serviceUrl);
        $rootScope.serviceUrl = $scope.setting.serviceUrl;
        $scope.back();
    }

    $scope.default = function() {
        $scope.setting.serviceUrl = 'http://pen1demo.ddns.net:6115';
        window.localStorage.setItem("serviceUrl",  $scope.setting.serviceUrl);
        $rootScope.serviceUrl = $scope.setting.serviceUrl;
    }

    $scope.back = function() {
        $state.go("login");
    }
})


.controller('LoginCtrl', function($scope, $stateParams, $state, $timeout, $http, $rootScope) {


    var username = window.localStorage.getItem("username");
    if (username != null && username != "") {
        $rootScope.username = username;
        $state.go("app.playlists");
    }

    $scope.loginData = {};
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);
        // $timeout(function() {
        //     $state.go("app.playlists");
        // }, 1000);

        $http.get($rootScope.serviceUrl + '/checkoutapi/api/login?user=' + $scope.loginData.username + '&password=' + $scope.loginData.password).
        success(function(data, status, headers, config) {
            console.log(data);
            // $cordovaToast.show('Login success', 'short', 'center'); 
            try {
                window.plugins.toast.show('Login success', 'short', 'bottom', function(a) {
                    console.log('toast success: ' + a)
                }, function(b) {
                    alert('toast error: ' + b)
                });
            } catch (e) {
                console.log(e);
            }
            window.localStorage.setItem("username", $scope.loginData.username);
            $rootScope.username = $scope.loginData.username;
            $state.go("app.playlists");
        }).
        error(function(data, status, headers, config) {
            console.log(data);
            // alert(status);
            try {
                window.plugins.toast.show('Login failed', 'short', 'bottom', function(a) {
                    console.log('toast success: ' + a)
                }, function(b) {
                    alert('toast error: ' + b)
                });
            } catch (e) {
                console.log(e);
            }

        });
    };


});
